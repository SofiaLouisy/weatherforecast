
/*var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
module.exports._test = {
  buildUrl,
  sendJSONRequest,
};*/

/*
* parameters in format [{ key: 'value1' }, { key: 'value1 value2' }]
*/
function buildUrl(base, parameters) {
  if (typeof parameters == 'undefined' || parameters==null) {
    return base;
  }

  var urlQueries = ['?'];
  
  for (obj of parameters) {
    for (key in obj) {
      urlQueries.push(key + '=' + obj[key].toString().trim().replace(/ /g,'+'));
      urlQueries.push('&');
    }
  }

  return base + urlQueries.slice(0,-1).join('');
}

function sendRequest(urlBase, method="GET", parameters=null, body=null, onSuccess=function(response){}, onFinished=function(){}) {
  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var response = this.responseText;

      onSuccess(response);
    }

    if (this.readyState == 4) {
      onFinished();
    }
  };

  xhttp.open(method, buildUrl(urlBase, parameters));
  xhttp.send(JSON.stringify(body));
}
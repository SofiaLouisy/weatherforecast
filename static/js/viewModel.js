/*
 * TODO:
 */

//#region user triggered functions

var geoCodeResponse;

// Close dropdowns if the user clicks outside of it
window.onclick = function (event) {
  if (!event.target.matches(".dropbtn")) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains("show")) {
        openDropdown.classList.remove("show");
      }
    }
  }
};

function findLocation() {
  address = document.getElementById("address").value;

  if (!address) {
    return;
  }

  sendRequest(
    (urlBase = "https://maps.googleapis.com/maps/api/geocode/json"),
    (method = "GET"),
    (parameters = [
      { address: address, key: "AIzaSyADv6rzsAGN0BUWAAyIueawps19rzEGjQ8" },
    ]),
    (body = null),
    (onSuccess = function (response) {
      geoCodeResponse = JSON.parse(response);
      var addresses = getFormattedAddresses(geoCodeResponse);

      emptyDropdown();

      addresses.forEach((formattedAddress) => {
        addToList(formattedAddress);
      });

      toggleDropdown();
    })
  );
}

function selectLocation(address) {
  var coord = getCoordinatesFromGeocodingResponse(
    geoCodeResponse,
    getIndex(address, geoCodeResponse)
  );

  validateCoordinates(
    (lat = coord[0]),
    (lon = coord[1]),
    (onSuccess = function () {
      emptyInputFields();

      createLocationLabel(coord[0], coord[1], address);

      document.getElementById("submitBtn").disabled = false;
    }),
    (onFail = function () {
      alert("Only addresses in Sweden are allowed");
    })
  );
}

function showForecast() {
  showLoader();

  sendRequest(
    (urlBase = "https://egz02i0hsf.execute-api.eu-west-1.amazonaws.com/dev"),
    (method = "POST"),
    (parameters = null),
    (body = getLocationObjects()),
    (onSuccess = (response) => {
      document
        .getElementById("image")
        .setAttribute("src", "data:image/png;base64," + response);
    }),
    (onFinished = showImage)
  );
}

//#endregion

//#region other functions

function addToList(address) {
  var li = document.createElement("li");
  li.appendChild(document.createTextNode(address));

  li.addEventListener("click", function (event) {
    selectLocation(event.target.innerHTML);
  });

  document.getElementById("addressList").appendChild(li);
}

function emptyDropdown() {
  node = document.getElementById("addressList");
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

function toggleDropdown() {
  document.getElementById("addressesDropdown").classList.toggle("show");
}

function emptyInputFields() {
  document.getElementById("form").reset();
}

function createLocationLabel(lat, lon, name) {
  var table = document.getElementById("locations");
  row = table.insertRow();

  cell1 = row.insertCell(0);
  cell1.innerHTML = name;

  cell2 = row.insertCell(1);
  cell2.innerHTML = lat;

  cell3 = row.insertCell(2);
  cell3.innerHTML = lon;

  cell4 = row.insertCell(3);
  cell4.innerHTML = "&times";
  cell4.classList.add("close");
  cell4.addEventListener("click", function (event) {
    table.deleteRow(event.target.parentElement.rowIndex);
  });
}

function getLocationObjects() {
  var allLocations = [];

  var rows = document.getElementById("locations").rows;

  for (i = 1; i < rows.length; i++) {
    allLocations.push({
      latitude: rows[i].cells[1].innerHTML,
      longitude: rows[i].cells[2].innerHTML,
      name: rows[i].cells[0].innerHTML,
    });
  }

  return allLocations;
}

function showLoader() {
  document.getElementById("image").style.display = "none";
  document.getElementById("loader").style.display = "block";
}

function showImage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("image").style.display = "block";
}

//#endregion

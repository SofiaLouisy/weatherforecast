/*module.exports._test = {
  getCoordinatesFromGeocodingResponse,
  validateCoordinates,
  isMultipleLocations,
  GetFormattedAddresses
};*/

function getFormattedAddresses(obj) {
  var addresses = [];

  obj["results"].forEach(element => {
    addresses.push(element["formatted_address"]);
  });

  return addresses;
}

function getIndex(formattedAddress, obj) {
  var addresses = getFormattedAddresses(obj);

  index = -1;

  for (i=0; i < addresses.length; i++) {
    if (formattedAddress == addresses[i]) {
      index = i;
      break;
    }
  }

  return index;
}

function getCoordinatesFromGeocodingResponse(obj, index=0) {
  var location = obj["results"][index]["geometry"]["location"];
  lat = Math.round(location['lat'] * 1000000) / 1000000
  lon = Math.round(location['lng'] * 1000000) / 1000000
  return [lat, lon];
}

function validateCoordinates(lat, lon, onSuccess=function(){}, onFail=function(){}) {
  if (lat < 52.500440 || lat > 70.666011 || lon < 2.250475 || lon > 27.392184) {
    onFail();
  }
  else {
    onSuccess();
  }
}
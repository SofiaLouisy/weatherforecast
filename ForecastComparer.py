from WeatherForecast import WeatherForecast
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import matplotlib.dates as mdates

class ForecastComparer:
    def __init__(self):
        self.forecasts = []
        self.forecastLocations = []

    def addLocation(self, lon, lat, name=""):
        if name == "":
            name = "{}, {}".format(lon, lat)

        self.forecasts.append(WeatherForecast(lon, lat, name))
        
        return self

    def savefig(self, filename, figformat=None):
        if len(self.forecasts) < 1:
            raise NoLocationException("No forecast to plot.")

        forecastKeys = list(self.forecasts[0].forecastPlottable.keys())
        forecastKeys.sort()

        fig, axs = plt.subplots(len(forecastKeys), figsize=(8,10))

        for i in range(len(forecastKeys)):
            for k in range(len(self.forecasts)):
                values = self.forecasts[k].forecastPlottable.get(forecastKeys[i])

                axs[i].plot(*values, 
                            label=self.forecasts[k].getName())

            axs[i].set_title(forecastKeys[i])
            axs[i].grid(True)
            axs[i].legend()
            axs[i].set_ylabel(WeatherForecast.getUnit(forecastKeys[i]))

            axs[i].xaxis.set_major_locator(mdates.DayLocator())
            axs[i].xaxis.set_major_formatter(mdates.DateFormatter('%a %d %b\n%H:%M'))
            axs[i].xaxis.set_minor_locator(mdates.HourLocator())

            for tick in axs[i].get_xticklabels():
                tick.set_rotation(45)
        
        plt.tight_layout()
        plt.savefig(filename, format=figformat)

class NoLocationException(Exception):
    pass

if __name__ == '__main__':
    ForecastComparer().addLocation(13.495992, 55.837480, "Snogeröd").addLocation(14.998077, 60.063478, "Grängesberg").savePng("forecast")
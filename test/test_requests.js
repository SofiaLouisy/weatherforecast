var assert = require('assert');
main = require('../static/js/requests.js');

console.log(main);

describe("Url build", function() {

  describe("build", function() {
    it("Builds a url with simple queries", function() {
      assert.equal(main._test.buildUrl('www.example.com', 
      [ { key1: 'value1' },
        { key2: 'value2' }]
      ),
        'www.example.com?key1=value1&key2=value2'
      )
    });
  });

  describe("build", function() {
    it("Builds a url with plural values", function() {
      assert.equal(main._test.buildUrl('www.example.com', 
      [ { key1: 'value1' },
        { key2: 'value21 value22' }]
      ),
        'www.example.com?key1=value1&key2=value21+value22'
      )
    });
  });

  describe("build", function() {
    it("Builds a url with equal keys", function() {
      assert.equal(main._test.buildUrl('www.example.com', 
      [ { key: 'value' },
        { key: 'value1 value2' }]
      ),
        'www.example.com?key=value&key=value1+value2'
      )
    });
  });
});

/*
module.exports._test = {
  buildUrl,
  getCoordinatesFromGeocodingResponse
};
*/
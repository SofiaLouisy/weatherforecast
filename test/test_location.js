var assert = require("assert");

var main = require("../static/js/location.js");

var response_single = {
  results: [
    {
      address_components: [
        {
          long_name: "1600",
          short_name: "1600",
          types: ["street_number"],
        },
        {
          long_name: "Amphitheatre Parkway",
          short_name: "Amphitheatre Pkwy",
          types: ["route"],
        },
        {
          long_name: "Mountain View",
          short_name: "Mountain View",
          types: ["locality", "political"],
        },
        {
          long_name: "Santa Clara County",
          short_name: "Santa Clara County",
          types: ["administrative_area_level_2", "political"],
        },
        {
          long_name: "California",
          short_name: "CA",
          types: ["administrative_area_level_1", "political"],
        },
        {
          long_name: "United States",
          short_name: "US",
          types: ["country", "political"],
        },
        {
          long_name: "94043",
          short_name: "94043",
          types: ["postal_code"],
        },
      ],
      formatted_address: "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",
      geometry: {
        location: {
          lat: 37.426786,
          lng: -122.080603,
        },
        location_type: "ROOFTOP",
        viewport: {
          northeast: {
            lat: 37.4281350802915,
            lng: -122.0792542197085,
          },
          southwest: {
            lat: 37.4254371197085,
            lng: -122.0819521802915,
          },
        },
      },
      place_id: "ChIJtYuu0V25j4ARwu5e4wwRYgE",
      plus_code: {
        compound_code: "CWC8+R3 Mountain View, California, United States",
        global_code: "849VCWC8+R3",
      },
      types: ["street_address"],
    },
  ],
  status: "OK",
};

var response_multiple = {
  results: [
    {
      address_components: [
        {
          long_name: "Havrevägen",
          short_name: "Havrevägen",
          types: ["route"],
        },
        {
          long_name: "Lyckeby",
          short_name: "Lyckeby",
          types: ["political", "sublocality", "sublocality_level_1"],
        },
        {
          long_name: "Lyckeby",
          short_name: "Lyckeby",
          types: ["postal_town"],
        },
        {
          long_name: "Blekinge län",
          short_name: "Blekinge län",
          types: ["administrative_area_level_1", "political"],
        },
        {
          long_name: "Sverige",
          short_name: "SE",
          types: ["country", "political"],
        },
        {
          long_name: "371 60",
          short_name: "371 60",
          types: ["postal_code"],
        },
      ],
      formatted_address: "Havrevägen, 371 60 Karlskrona, Sverige",
      geometry: {
        bounds: {
          northeast: {
            lat: 56.1985045,
            lng: 15.6780664,
          },
          southwest: {
            lat: 56.1980435,
            lng: 15.67462,
          },
        },
        location: {
          lat: 56.19822929999999,
          lng: 15.6757902,
        },
        location_type: "GEOMETRIC_CENTER",
        viewport: {
          northeast: {
            lat: 56.19962298029149,
            lng: 15.6780664,
          },
          southwest: {
            lat: 56.1969250197085,
            lng: 15.67462,
          },
        },
      },
      place_id: "ChIJX8OphVZrVkYRVCagJKSiXnw",
      types: ["route"],
    },
    {
      address_components: [
        {
          long_name: "Havrevägen",
          short_name: "Havrevägen",
          types: ["route"],
        },
        {
          long_name: "Torvalla",
          short_name: "Torvalla",
          types: ["political", "sublocality", "sublocality_level_1"],
        },
        {
          long_name: "Östersund",
          short_name: "Östersund",
          types: ["postal_town"],
        },
        {
          long_name: "Jämtlands län",
          short_name: "Jämtlands län",
          types: ["administrative_area_level_1", "political"],
        },
        {
          long_name: "Sverige",
          short_name: "SE",
          types: ["country", "political"],
        },
        {
          long_name: "831 74",
          short_name: "831 74",
          types: ["postal_code"],
        },
      ],
      formatted_address: "Havrevägen, 831 74 Östersund, Sverige",
      geometry: {
        bounds: {
          northeast: {
            lat: 63.14658240000001,
            lng: 14.7567496,
          },
          southwest: {
            lat: 63.14426359999999,
            lng: 14.7479632,
          },
        },
        location: {
          lat: 63.14565700000001,
          lng: 14.7515187,
        },
        location_type: "GEOMETRIC_CENTER",
        viewport: {
          northeast: {
            lat: 63.1467719802915,
            lng: 14.7567496,
          },
          southwest: {
            lat: 63.14407401970849,
            lng: 14.7479632,
          },
        },
      },
      place_id: "ChIJ_SGW-dW4b0YRlrcFE4YBvbQ",
      types: ["route"],
    },
  ],
  status: "OK",
};

describe("Geocoding", function () {
  describe("API response", function () {
    it("Returns formatted address", function () {
      assert.deepStrictEqual(
        main._test.getFormattedAddresses(response_multiple),
        [
          "Havrevägen, 371 60 Karlskrona, Sverige",
          "Havrevägen, 831 74 Östersund, Sverige",
        ]
      );
    });
  });

  describe("API response", function () {
    it("Returns index of formatted address", function () {
      assert.equal(
        main._test.getIndex(
          "Havrevägen, 831 74 Östersund, Sverige",
          response_multiple
        ),
        1
      );
    });
  });

  describe("API response", function () {
    it("Returns lat, lon from Geocoding JSON object", function () {
      assert.deepStrictEqual(
        main._test.getCoordinatesFromGeocodingResponse(response_single),
        [37.426786, -122.080603]
      );
      assert.deepStrictEqual(
        main._test.getCoordinatesFromGeocodingResponse(response_multiple, 1),
        [63.145657, 14.751519]
      );
    });
  });
});

describe("validate coordinates", function () {
  var i = 0;

  function onSuccess() {
    i = 1;
  }
  function onFail() {
    i = -1;
  }

  describe("valid coordinates", function () {
    it("validates that acceptable coordinates are accepted", function () {
      i = 0;
      main._test.validateCoordinates(52.50044, 2.250475, onSuccess, onFail);
      assert.equal(i, 1);

      i = 0;
      main._test.validateCoordinates(52.50044, 27.392184, onSuccess, onFail);
      assert.equal(i, 1);

      i = 0;
      main._test.validateCoordinates(70.666011, 2.250475, onSuccess, onFail);
      assert.equal(i, 1);

      i = 0;
      main._test.validateCoordinates(70.666011, 27.392184, onSuccess, onFail);
      assert.equal(i, 1);
    });
  });
});

/*
module.exports._test = {
  buildUrl,
  getCoordinatesFromGeocodingResponse
};
*/

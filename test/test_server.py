import os
import unittest
import sys
import json

sys.path.append('.')

from WeatherService import app
from WeatherService import verify_location_data
from WeatherService import BadRequest

class BasicTests(unittest.TestCase):
    
    ############################
    #### setup and teardown ####
    ############################
    
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)
    
    # executed after each test
    def tearDown(self):
        pass
    
    
    ###############
    #### tests ####
    ###############
 
    def test_main_page(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.app.post('/',data=json.dumps([{'latitude': 55.3, 'longitude': 14, 'name': 'hejhej'}]))
        self.assertEqual(response.status_code, 200)
        print(dir)
        print(type(response.data))
        self.assertEqual(type(response.data), bytes)

    def test_verify_ok_location_data(self):
        self.assertEqual(verify_location_data({'latitude': 55.3, 'longitude': 14, 'name': 'hejhej'}), None)
        self.assertEqual(verify_location_data({'latitude': 55, 'longitude': 14.5}), None)

    def test_verify_bad_location_data(self):
        with self.assertRaises(BadRequest):
            verify_location_data({})

        with self.assertRaises(BadRequest):
            verify_location_data({'latitude': 'a', 'longitude': 14})
        
        with self.assertRaises(BadRequest):
            verify_location_data({'latitude': 55, 'longitude': 'a'})

        with self.assertRaises(BadRequest):
            verify_location_data({'latitude': 55, 'longitude': 14, 'name':15})

        with self.assertRaises(BadRequest):
            verify_location_data({'longitude': 14})


if __name__ == "__main__":
    unittest.main()
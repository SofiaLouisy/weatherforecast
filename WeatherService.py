from flask import Flask, render_template, request, make_response
from flask_assets import Bundle, Environment
from werkzeug.exceptions import BadRequest
from ForecastComparer import ForecastComparer
from io import BytesIO
import base64
import json

app = Flask(__name__)

assets = Environment(app)

js = Bundle('js/location.js', 'js/requests.js', 'js/viewModel.js',
            filters='jsmin', output='gen/packed.js')

assets.register('js_all', js)

exception_message = 'Requestdata must be of format "[{"latitude": {float}, "longitude": {float}, "name": {string}}, ...]"'

@app.route('/', methods=['GET', 'POST'])
def hello_world():
    if request.method == 'GET':
        return render_template("index.html")

    else:
        print(request.form)
        print(request.args)

        data = json.loads(request.data)

        print(data)

        if type(data) != list:
            raise BadRequest(exception_message)

        if len(data) == 0:
            return ''.encode()

        comparer = ForecastComparer()

        for location in data:

            verify_location_data(location)

            name = location.get('name', '')

            comparer.addLocation(lon=float(location['longitude']), 
                                lat=float(location['latitude']),
                                name=name)
        
        imageStream = BytesIO()
        comparer.savefig(imageStream, figformat="png")

        imageStream.seek(0)

        imageBase64 = base64.b64encode(imageStream.getvalue()).decode()

        return imageBase64

def verify_location_data(data):
    if 'latitude' not in data or 'longitude' not in data:
        raise BadRequest(exception_message)

    if 'name' in data and type(data['name']) is not str:
        raise BadRequest(exception_message)

    try:
        float(data['latitude'])
        float(data['longitude'])
    except ValueError:
        raise BadRequest(exception_message)


# note: must update forecastcomparer to traverse argument vector
# use utime to know what takes time. Gunicorn to check this.

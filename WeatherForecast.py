from data.SMHIdata import SMHIdata
from resources.parameterTable import PROPERTY_UNIT_TABLE
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import matplotlib.dates as mdates

class WeatherForecast:
    """
    Weather forecast bound by position {longitude}, {latitude}
    """
    def __init__(self, longitude, latitude, name="", sourceFunction=SMHIdata()):
        if name == "":
            name = "{}, {}".format(longitude, latitude)

        self.longitude = longitude
        self.latitude = latitude
        self.name = name
        self.sourceFunction = sourceFunction
        self.i = -1

        self.forecast = self.__getForecast()
        self.forecastPlottable = self.__forecastPlottableFormat()

    @staticmethod
    def getUnit(weatherProperty):
        return PROPERTY_UNIT_TABLE.get(weatherProperty, "")

    def getName(self):
        return self.name

    def getRank(weatherProperty):
        return np.mean(self.forecastPlottable[weatherProperty])

    def __getForecast(self):
        return self.sourceFunction(self.longitude, self.latitude)

    def __forecastPlottableFormat(self):
        parsedData = {}

        for weatherAtTime in self.forecast:
            for weatherProperty in weatherAtTime[1]:
                if not parsedData.get(weatherProperty):
                    parsedData[weatherProperty] = [], []

                parsedData.get(weatherProperty)[0].append(weatherAtTime[0])
                parsedData.get(weatherProperty)[1].append(
                        weatherAtTime[1][weatherProperty])
        
        return parsedData

    # "Official" string representation of an object
    def __repr__(self):
        return self.__str__()
    
    # "Informal" or nicely printable string
    def __str__(self):
        myStr = self.__doc__.format(longitude=self.longitude, latitude=self.latitude)

        for date, prediction in self.forecast:
            myStr += "\n{}  -  ".format(date)

            for weatherProperty in prediction:
                myStr += "{}: {}   ".format(weatherProperty, prediction[weatherProperty])
        
        return myStr

    def __iter__(self):
        return self

    def __next__(self):
        try:
            self.i += 1
            return self.forecast[self.i]
        except IndexError:
            raise StopIteration()

    def __gt__(self, other):
        return np.mean(self.forecastPlottable["Air temperature"]) > np.mean(other.forecastPlottable["Air temperature"])

    def __eq__(self, other):
        if other == None:
            return False

        return np.mean(self.forecastPlottable["Air temperature"]) > np.mean(other.forecastPlottable["Air temperature"])

    def savePng(self, filename, format='png'):
        forecastKeys = list(self.forecastPlottable.keys())
        forecastKeys.sort()

        fig, axs = plt.subplots(len(forecastKeys), figsize=(8,10))

        for i in range(len(forecastKeys)):
            values = self.forecastPlottable.get(forecastKeys[i])

            axs[i].plot(*values, 
                        label=self.name)

            axs[i].set_title(forecastKeys[i])
            axs[i].grid(True)
            axs[i].legend()
            axs[i].set_ylabel(WeatherForecast.getUnit(forecastKeys[i]))

            axs[i].xaxis.set_major_locator(mdates.DayLocator())
            axs[i].xaxis.set_major_formatter(mdates.DateFormatter('%a %d %b\n%H:%M'))
            axs[i].xaxis.set_minor_locator(mdates.HourLocator())

            for tick in axs[i].get_xticklabels():
                tick.set_rotation(45)
        
        plt.tight_layout()
        plt.savefig(filename, format="png")

if __name__ == "__main__":
    pass
    WeatherForecast(13,55,"test").savePng("test.png")


    """
    import matplotlib.pyplot as plt
    snogerod = 13.495992, 55.837480
    grangesberg = 14.998077, 60.063478

    x = WeatherForecast(*snogerod)
    y = WeatherForecast(*grangesberg)



    print(x)

    plt.plot(*x.forecastPlottable.get("Air temperature"), label="Snogeröd")
    plt.plot(*y.forecastPlottable.get("Air temperature"), label="Grängesberg")
    plt.legend()
    plt.show()
    """

#datetime och value i iter next
#for date, value in weatherfrcst
#printa snyggt och beskrivande.
#def __str__(self):
#   return self.__doc__ - ger headern för klassen
from config.SMHIconfig import *
import requests
from datetime import datetime

class SMHIdata:
    def __init__(self):
        pass

    def __call__(self, longitude, latitude):
        return self.getData(longitude, latitude)

    def getData(self, longitude, latitude):
        response = requests.get(self.__getUrl(longitude, latitude))
        self.data = response.json()

        return self.__parse()

    def __getUrl(self, longitude, latitude):
        url = URL.format(url_endpoint=URL_ENDPOINT,
                           category=CATEGORY,
                           version=VERSION,
                           geotype=GEOTYPE,
                           longitude=longitude,
                           latitude=latitude)
        print(url)
        
        return  url

    def __parse(self):
        parsedData = []

        timeSeries = self.data["timeSeries"]

        for wheatherAtTime in timeSeries:

            parsedData.append((datetime.strptime(wheatherAtTime["validTime"], "%Y-%m-%dT%H:%M:%SZ"), {}))

            for responseParameter in wheatherAtTime["parameters"]:

                parsedName = self.__convertTerm(responseParameter["name"])

                if parsedName == None:
                    continue

                parsedData[-1][1][parsedName] = responseParameter["values"][0]
        
        return parsedData

    def __convertTerm(self, term):
        if TERMINOLOGY_CONVERTION_TABLE.get(term):
            return TERMINOLOGY_CONVERTION_TABLE.get(term)[0]

    def __propertyFormat(self):
        return [],[]

    def legacy(self):

        parsedData = {}

        timeSeries = self.data["timeSeries"]

        for wheatherAtTime in timeSeries:
            for responseParameter in wheatherAtTime["parameters"]:
                parsedName = self.__convertTerm(responseParameter["name"])

                if parsedName == None:
                    continue

                if not parsedData.get(parsedName):
                    parsedData[parsedName] = self.__propertyFormat()

                parsedData.get(parsedName)[0].append(
                        datetime.strptime(wheatherAtTime["validTime"], "%Y-%m-%dT%H:%M:%SZ"))
                parsedData.get(parsedName)[1].append(
                        responseParameter["values"][0])
        
        return parsedData
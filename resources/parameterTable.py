PROPERTY_UNIT_TABLE = {
    "Air temperature": "C",
    "Wind speed": "m/s",
    "Mean precipitation intensity": "mm/h"
}
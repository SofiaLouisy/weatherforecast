URL_ENDPOINT        = "https://opendata-download-metfcst.smhi.se"
URL                 = "{url_endpoint}/api/category/{category}/version/{version}/geotype/{geotype}/lon/{longitude}/lat/{latitude}/data.json"
VERSION             = "2"
CATEGORY            = "pmp3g"
GEOTYPE             = "point"

TERMINOLOGY_CONVERTION_TABLE = {
    't': ("Air temperature", "C"),
    'ws': ("Wind speed", "m/s"),
    'pmean': ("Mean precipitation intensity", "mm/s")
}